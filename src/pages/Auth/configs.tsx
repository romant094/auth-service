import { Button } from '@components/Button'
import { TimerButton, LinkButton, Sign, PhoneInput, AgreementCheckbox, CodeField, PasswordField } from './components'

import {
  authActions,
  handleLogin,
  handleGetVerificationCode,
  handleVerifyPhone,
  handleRegister,
  handleRequestPasswordRecovery,
  handleVerifyPasswordRecoveryCode,
  handleChangePassword,
} from '@app/redux/modules/auth'
import { NamePath } from '../../router/type'
import { ERRORS } from './errors'

const validatePhone = (phone) => {
  return /^\+\d{11,16}$/.test(phone)
}

const validateVerificationCode = (code) => {
  return /^\d{4}$/.test(code)
}

const getPhoneNumber = (state) => state.phone_number.replace(/[ ()-]/g, '')

const phoneInputFieldConfig = (auth, dispatch, hasLabel = false) => ({
  props: {
    helperText: auth.errors.phone && ERRORS.wrongPhone,
    error: auth.errors.phone,
    hasLabel,
    onChange() {
      dispatch(authActions.setError({ field: 'phone', value: false }))
    },
  },
  component: PhoneInput,
})

const onSetPassword = (e, dispatch, fieldName) => {
  const { value } = e.target
  dispatch(authActions.updateField({ name: fieldName, value }))
  dispatch(authActions.setError({ field: fieldName, value: false }))
}

const loginConfig = (auth, dispatch) => ({
  header: 'Вход',
  fieldSet: [
    phoneInputFieldConfig(auth, dispatch),
    {
      props: {
        label: 'Пароль',
        placeholder: 'Введите пароль',
        name: 'password',
        errorText:
          (auth.errors.emptyPassword && ERRORS.emptyPassword) || (auth.errors.password && ERRORS.wrongPassword),
        onChange(e) {
          const { name, value } = e.target
          ;['emptyPassword', 'password'].forEach((field) => {
            dispatch(authActions.setError({ field, value: false }))
          })
          dispatch(authActions.updateField({ name, value }))
        },
      },
      component: PasswordField,
    },
    {
      props: {
        type: 'primary-blue',
        children: 'Войти',
        onClick: async () => {
          const { password } = auth
          const phone = getPhoneNumber(auth)
          const data = { phone_number: phone, password }
          if (!validatePhone(phone)) {
            dispatch(authActions.setError({ field: 'phone', value: true }))
            return
          }
          if (!password) {
            dispatch(authActions.setError({ field: 'emptyPassword', value: true }))
            return
          }
          const hasErrors = Object.values(auth.errors).some((item) => item)
          if (!hasErrors) {
            dispatch(handleLogin(data))
          }
        },
      },
      component: Button,
    },
    {
      props: {
        buttonType: 'secondary-blue',
        text: 'Зарегистрироваться',
        path: NamePath.REGISTER,
      },
      component: LinkButton,
    },
    {
      props: {
        buttonType: 'naked-blue',
        text: 'Забыли пароль?',
        path: NamePath.PASSWORD_RECOVERY,
      },
      component: LinkButton,
    },
  ],
})

const registerPhoneConfig = (auth, dispatch) => ({
  header: 'Регистрация',
  fieldSet: [
    phoneInputFieldConfig(auth, dispatch),
    {
      props: {
        children: 'Мы пришлем вам код по СМС',
      },
      component: Sign,
    },
    {
      props: {},
      component: AgreementCheckbox,
    },
    {
      props: {
        type: 'primary-blue',
        children: 'Далее',
        disabled: !auth.checkboxChecked,
        onClick() {
          const phone = getPhoneNumber(auth)
          if (!validatePhone(phone)) {
            dispatch(authActions.setError({ field: 'phone', value: true }))
            return
          }
          dispatch(handleGetVerificationCode(phone))
        },
      },
      component: Button,
    },
    {
      props: {
        buttonType: 'secondary-blue',
        text: 'У меня уже есть данные для входа',
        path: NamePath.LOGIN,
      },
      component: LinkButton,
    },
    {
      props: {
        buttonType: 'naked-blue',
        text: 'Забыли пароль?',
        path: NamePath.PASSWORD_RECOVERY,
      },
      component: LinkButton,
    },
  ],
})

const registerPasswordConfig = (auth, dispatch) => ({
  header: 'Регистрация',
  fieldSet: [
    {
      props: {
        label: 'Придумайте пароль',
        placeholder: 'Не менее 8 символов',
        errorText: auth.errors.password && ERRORS.wrongPassword,
        onChange: (e) => onSetPassword(e, dispatch, 'password'),
      },
      component: PasswordField,
    },
    {
      props: {
        label: 'Подтвердите пароль',
        placeholder: 'Пароль',
        errorText: auth.errors.confirmPassword && ERRORS.passwordsDoNotMatch,
        onChange: (e) => onSetPassword(e, dispatch, 'confirmPassword'),
      },
      component: PasswordField,
    },
    {
      props: {
        type: 'primary-blue',
        children: 'Зарегистрироваться',
        onClick() {
          const { phone_number: phone, password, confirmPassword } = auth
          if (password.length < 8) {
            dispatch(authActions.setError({ field: 'password', value: true }))
            return
          }
          if (password !== confirmPassword) {
            dispatch(authActions.setError({ field: 'confirmPassword', value: true }))
            return
          }
          dispatch(handleRegister({ phone_number: phone, password }))
        },
      },
      component: Button,
    },
    {
      props: {
        type: 'naked-blue',
        children: 'Назад',
        onClick() {
          dispatch(authActions.setCurrentStep(1))
        },
      },
      component: Button,
    },
  ],
})

const passwordRecoveryPhoneConfig = (auth, dispatch) => ({
  header: 'Восстановление пароля',
  fieldSet: [
    phoneInputFieldConfig(auth, dispatch, true),
    {
      props: {
        type: 'primary-blue',
        children: 'Далее',
        disabled: !auth.checkboxChecked,
        onClick() {
          const phone = getPhoneNumber(auth)
          if (!validatePhone(phone)) {
            dispatch(authActions.setError({ field: 'phone', value: true }))
            return
          }
          dispatch(handleRequestPasswordRecovery(phone))
        },
      },
      component: Button,
    },
    {
      props: {
        buttonType: 'naked-blue',
        text: 'Я вспомнил пароль',
        path: NamePath.LOGIN,
      },
      component: LinkButton,
    },
  ],
})

const codeConfirmationConfig = (auth, dispatch) => (header, action) => ({
  header,
  fieldSet: [
    {
      component: CodeField,
    },
    {
      props: {
        type: 'primary-blue',
        children: 'Подтвердить номер',
        onClick() {
          const { verificationCode: code, phone_number: phone } = auth
          if (!validateVerificationCode(code)) {
            dispatch(authActions.setError({ field: 'verificationCode', value: true }))
            return
          }
          dispatch(action({ phone, code }))
        },
      },
      component: Button,
    },
    {
      props: {
        type: 'secondary-blue',
      },
      component: TimerButton,
    },
    {
      props: {
        type: 'naked-blue',
        children: 'Назад',
        onClick() {
          dispatch(authActions.setCurrentStep(0))
        },
      },
      component: Button,
    },
  ],
})

const passwordRecoveryCodeConfig = (auth, dispatch) =>
  codeConfirmationConfig(auth, dispatch)('Восстановление пароля', handleVerifyPasswordRecoveryCode)
const registerCodeConfig = (auth, dispatch) => codeConfirmationConfig(auth, dispatch)('Регистрация', handleVerifyPhone)

const passwordRecoveryPasswordConfig = (auth, dispatch) => ({
  header: 'Восстановление пароля',
  fieldSet: [
    {
      props: {
        label: 'Придумайте новый пароль',
        placeholder: 'Новый пароль',
        errorText: auth.errors.password && ERRORS.wrongPassword,
        onChange: (e) => onSetPassword(e, dispatch, 'password'),
      },
      component: PasswordField,
    },
    {
      props: {
        label: 'Подтвердите новый пароль',
        placeholder: 'Подтвердите новый пароль',
        errorText: auth.errors.confirmPassword && ERRORS.passwordsDoNotMatch,
        onChange: (e) => onSetPassword(e, dispatch, 'confirmPassword'),
      },
      component: PasswordField,
    },
    {
      props: {
        type: 'primary-blue',
        children: 'Сохранить новый пароль',
        onClick() {
          const { passwordRecoverSecretKey, password, confirmPassword } = auth
          if (password.length < 8) {
            dispatch(authActions.setError({ field: 'password', value: true }))
            return
          }
          if (password !== confirmPassword) {
            dispatch(authActions.setError({ field: 'confirmPassword', value: true }))
            return
          }
          dispatch(handleChangePassword({ secret: passwordRecoverSecretKey, new_password: password }))
        },
      },
      component: Button,
    },
  ],
})

export interface IAuthFormConfigItem {
  path: string
  step?: number
  config?: any
  getConfig?: any
}

export const authFormConfig: IAuthFormConfigItem[] = [
  {
    path: NamePath.REGISTER,
    step: 0,
    getConfig: registerPhoneConfig,
  },
  {
    path: NamePath.REGISTER,
    step: 1,
    getConfig: registerCodeConfig,
  },
  {
    path: NamePath.REGISTER,
    step: 2,
    getConfig: registerPasswordConfig,
  },
  {
    path: NamePath.LOGIN,
    getConfig: loginConfig,
  },
  {
    path: NamePath.PASSWORD_RECOVERY,
    step: 0,
    getConfig: passwordRecoveryPhoneConfig,
  },
  {
    path: NamePath.PASSWORD_RECOVERY,
    step: 1,
    getConfig: passwordRecoveryCodeConfig,
  },
  {
    path: NamePath.PASSWORD_RECOVERY,
    step: 2,
    getConfig: passwordRecoveryPasswordConfig,
  },
]
