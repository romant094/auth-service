import React, { useMemo, useEffect } from 'react'
import { FormGroup } from '@mui/material'
import Banner from '../../assets/images/Login/onbording.jpg'

import { useLocation, useHistory } from 'react-router-dom'
import { useAppSelector, useAppDispatch } from '@app/redux/hooks'
import { authFormConfig } from './configs'
import styles from './style.module.scss'
import { authActions } from '@app/redux/modules/auth'
import { NamePath } from '../../router/type'

const AuthTemplate = ({ getConfig }) => {
  const auth = useAppSelector((store) => store.auth)
  const dispatch = useAppDispatch()
  const config = getConfig(auth, dispatch)
  return (
    <div
      className={styles.content}
      style={{
        backgroundImage: `url(${Banner})`,
      }}
    >
      <div className='main-container'>
        <div className={styles.container}>
          <h1 className={styles.title}>{config.header}</h1>
          <form className={styles.form}>
            {config.fieldSet.map((item, index) => {
              const { component: Component, props } = item
              return (
                <FormGroup key={index}>
                  <Component {...props} />
                </FormGroup>
              )
            })}
          </form>
        </div>
      </div>
    </div>
  )
}

const AuthForm = () => {
  const history = useHistory()
  const dispatch = useAppDispatch()
  const location = useLocation()
  const { isAuth, step, redirect } = useAppSelector((store) => store.auth)

  const { getConfig } = useMemo(
    () =>
      authFormConfig.find((item) => {
        const matchStep = 'step' in item ? item.step === step : true
        const matchLocation = item.path === location.pathname
        return matchLocation && matchStep
      }),
    [step, location],
  )

  useEffect(() => {
    dispatch(authActions.clearAuthData())
  }, [location.pathname])

  if (redirect) {
    dispatch(authActions.updateField({ name: 'redirect', value: '' }))
    history.push(redirect)
  }

  if (isAuth) {
    history.push(NamePath.MAIN)
  }

  return <AuthTemplate getConfig={getConfig} />
}

export default AuthForm
