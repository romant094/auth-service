export enum ERRORS {
  wrongPassword = 'Длина пароля должна быть не менее 8 символов.',
  wrongPhone = 'Некорректный номер телефона.',
  passwordsDoNotMatch = 'Пароли не совпадают.',
  emptyPassword = 'Пароль не может быть пустым.',
  verificationCode = 'Код должен содержать 4 цифры.',
}

export enum API_ERRORS {}
