import { useAppDispatch, useAppSelector } from '@app/redux/hooks'
import { authActions } from '@app/redux/modules/auth'
import MuiPhoneNumber from 'material-ui-phone-number'
import React from 'react'
import style from './style.module.scss'

export const PhoneInput = ({ name, onChange, hasLabel = false, ...props }) => {
  const fieldName = name || 'phone_number'
  const dispatch = useAppDispatch()
  const { phone_number: phone } = useAppSelector((store) => store.auth)

  const handleChange = (value) => {
    onChange && onChange()
    dispatch(authActions.updateField({ name: fieldName, value }))
  }

  return (
    <>
      {hasLabel && (
        <label className={style.label} htmlFor='phone'>
          Номер телефона
        </label>
      )}
      <MuiPhoneNumber
        id='phone'
        name={fieldName}
        variant='filled'
        value={phone}
        onChange={handleChange}
        defaultCountry='ru'
        onlyCountries={['ru']}
        countryCodeEditable={false}
        disableDropdown
        {...props}
      />
    </>
  )
}
