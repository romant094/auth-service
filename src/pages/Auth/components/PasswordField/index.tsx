import React, { useState } from 'react'
import classNames from 'classnames'
import { VisibilityOutlined, VisibilityOffOutlined } from '@mui/icons-material'
import { TextInput } from '../TextInput'
import style from './style.module.scss'

export const PasswordField = ({ label, errorText, ...props }) => {
  const [passwordVisible, setPasswordVisible] = useState(false)
  const Icon = passwordVisible ? VisibilityOffOutlined : VisibilityOutlined
  const iconProps = {
    onClick(e) {
      e.preventDefault()
      e.stopPropagation()
      setPasswordVisible((prev) => !prev)
    },
    className: classNames(style.icon, { [style.error]: !!errorText }),
  }
  const component = <Icon {...iconProps} />
  const fieldType = passwordVisible ? 'text' : 'password'

  return <TextInput label={label} addon={component} type={fieldType} errorText={errorText} {...props} />
}
