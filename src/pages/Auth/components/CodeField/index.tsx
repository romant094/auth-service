import { useAppSelector, useAppDispatch } from '@app/redux/hooks'
import { TextInput } from '../TextInput'
import React from 'react'
import { authActions } from '@app/redux/modules/auth'
import { ERRORS } from '../../errors'

export const CodeField = () => {
  const dispatch = useAppDispatch()
  const { phone_number: phone, errors } = useAppSelector((store) => store.auth)
  const { verificationCode } = errors

  const label = (
    <span>
      Введите код, отправленный на номер:
      <br />
      {phone}
    </span>
  )

  const onChange = (e) => {
    const { value } = e.target
    dispatch(authActions.setError({ field: 'verificationCode', value: false }))
    dispatch(authActions.updateField({ name: 'verificationCode', value }))
  }

  return (
    <TextInput
      errorText={verificationCode && ERRORS.verificationCode}
      onChange={onChange}
      label={label}
      placeholder='Код из смс'
      id='code'
    />
  )
}
