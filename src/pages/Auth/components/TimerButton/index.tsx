import React, { useState, useRef, useMemo, useEffect } from 'react'
import { Button } from '@components/Button'
import { useAppDispatch, useAppSelector } from '@app/redux/hooks'
import { handleGetVerificationCode } from '@app/redux/modules/auth'

export const TimerButton: React.FC<any> = ({ type }) => {
  const dispatch = useAppDispatch()
  const phone = useAppSelector((state) => state.auth.phone_number)
  const INITIAL_TIMER = 150
  const [secondsLeft, setSecondsLeft] = useState(INITIAL_TIMER)
  const timer = useRef(null)
  const enabled = useMemo(() => secondsLeft === 0, [secondsLeft])
  const disabled = useMemo(() => !enabled, [enabled])

  const startTimer = () => {
    setSecondsLeft(INITIAL_TIMER)
    timer.current = setInterval(() => {
      setSecondsLeft((prev) => prev - 1)
    }, 1000)
  }

  const handleClick = () => {
    enabled && dispatch(handleGetVerificationCode(phone))
    startTimer()
  }

  const formatTime = (time) => {
    const min = Math.floor(time / 60)
    const sec = time % 60
    return `${min}:${sec < 10 ? `0${sec}` : sec}`
  }

  useEffect(() => {
    startTimer()
    return () => {
      clearInterval(timer.current)
    }
  }, [])

  useEffect(() => {
    if (enabled) {
      clearInterval(timer.current)
    }
  }, [secondsLeft])

  return (
    <Button type={type} disabled={disabled} onClick={handleClick}>
      {`Отправить код повторно${disabled ? ` через ${formatTime(secondsLeft)}` : ''}`}
    </Button>
  )
}
