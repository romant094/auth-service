import React from 'react'
import { FilledInput } from '@mui/material'
import classNames from 'classnames'
import style from './style.module.scss'

interface ITextInput {
  label?: any
  addon?: any
  type?: string
  id?: string
  placeholder?: string
  errorText?: string
  value?: string
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void
}

export const TextInput = ({ label, addon, type = 'text', errorText, ...props }: ITextInput) => {
  const classes = classNames(style.container, { [style.hasError]: !!errorText })
  return (
    <div className={style.outerContainer}>
      {label && (
        <label className={style.label} htmlFor={props.id}>
          {label}
        </label>
      )}
      <div className={style.innerContainer}>
        <FilledInput className={classes} type={type} autoComplete='off' {...props} />
        {addon}
      </div>
      {errorText && <div className={style.error}>{errorText}</div>}
    </div>
  )
}
