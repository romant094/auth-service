import React from 'react'

export const Sign: React.FC<any> = ({ children, className }) => {
  return <p className={className}>{children}</p>
}
