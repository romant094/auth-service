import { Link } from 'react-router-dom'
import { Button } from '@components/Button'
import React from 'react'

export const LinkButton = ({ path, buttonType, text }) => (
  <Link to={path}>
    <Button type={buttonType}>{text}</Button>
  </Link>
)
