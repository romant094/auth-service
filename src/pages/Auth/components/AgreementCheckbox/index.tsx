import React from 'react'
import { FormControlLabel, Checkbox } from '@mui/material'
import pdf from './iss-api-rus-v14.pdf'
import { useAppSelector, useAppDispatch } from '@app/redux/hooks'
import { authActions } from '@app/redux/modules/auth'

export const AgreementCheckbox = () => {
  const { checkboxChecked } = useAppSelector((store) => store.auth)
  const dispatch = useAppDispatch()
  const handleChange = () => {
    dispatch(authActions.setCheckbox())
  }
  const agreementLink = (
    <a href={pdf} rel='nofollow noreferrer' target='_blank'>
      условиями обработки персональных данных
    </a>
  )

  const label = (
    <span>
      Заполняя форму, я соглашаюсь с {agreementLink} и подтверждаю, что я владелец указанного номера телефона.
    </span>
  )

  const checkbox = <Checkbox checked={checkboxChecked} onChange={handleChange} />

  return <FormControlLabel control={checkbox} label={label} />
}
