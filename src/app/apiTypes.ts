// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace apiTypes {
  export type Token = string
  export type Phone = string
  export interface CredentialsCode {
    // eslint-disable-next-line camelcase
    phone: Phone
    code: string
  }
  export type CredentialsPassword = {
    // eslint-disable-next-line camelcase
    phone_number: string
    password: string
  }
  export type PasswordRecover = {
    // eslint-disable-next-line camelcase
    new_password: string
    secret: string
  }
}
