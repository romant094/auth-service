import { api as apiClient } from '@app/api/apiClient'

type Token = {
  access: string
  refresh: string
}

export const setToken: (token: Token) => void = (token) => {
  localStorage.setItem('token', JSON.stringify(token))
  apiClient.setUserToken(token.access)
}

export const removeToken: () => void = () => {
  localStorage.removeItem('token')
  apiClient.setUserToken()
}
