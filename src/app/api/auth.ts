import { api } from './apiClient'

export const login = async (data): Promise<any> => {
  const res = await api.post('/auth/login', data)
  return res.data
}

export const refreshToken = async (refresh): Promise<any> => {
  const res = await api.post('/auth/token/refresh', { refresh })
  return res.data
}

export const getVerificationCode = async (phone): Promise<any> => {
  return await api.post('/auth/phone/verify', { phone })
}

export const verifyPhone = async (data): Promise<any> => {
  return await api.put('/auth/phone/verify', data)
}

export const register = async (data): Promise<any> => {
  const res = await api.post('/auth/register', data)
  return res.data
}

export const logout = async (refresh): Promise<any> => {
  return await api.post('/auth/logout', { refresh_token: refresh })
}

export const passwordRecoverRequest = async (phone): Promise<any> => {
  return await api.post('/auth/password/recover-request', { phone })
}

export const passwordRecoverCodeVerification = async (data): Promise<any> => {
  return await api.post('/auth/password/recover-key', data)
}

export const changePassword = async (data): Promise<any> => {
  return await api.post('/auth/password/recover', data)
}
