import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import * as api from '@app/api/auth'
import { apiTypes } from '@app/apiTypes'
import { NamePath } from '../../../router/type'
import { setToken, removeToken } from '@app/api/helpers'

const token = JSON.parse(localStorage.getItem('token')) || {}

const initialState = {
  step: 0,
  sendCodeDisabled: true,
  redirect: '',
  passwordRecoverSecretKey: '',
  phone_number: '',
  password: '',
  verificationCode: '',
  confirmPassword: '',
  checkboxChecked: true,
  isAuth: !!token.access,
  token,
  errors: {
    emptyPassword: false,
    password: false,
    confirmPassword: false,
    phone: false,
    verificationCode: false,
  },
}

export const handleLogin = createAsyncThunk('auth/login', async (data: apiTypes.CredentialsPassword, { dispatch }) => {
  const token = await api.login(data)
  setToken(token)
  return token
})

export const handleRefreshToken = createAsyncThunk('auth/refreshToken', async (refresh: apiTypes.Token) => {
  try {
    const { access } = await api.refreshToken(refresh)
    setToken({ access, refresh })
    return access
  } catch (e) {
    removeToken()
  }
})

export const handleGetVerificationCode = createAsyncThunk('auth/getVerificationCode', async (phone: apiTypes.Phone) => {
  await api.getVerificationCode(phone)
})

export const handleVerifyPhone = createAsyncThunk('auth/verifyPhone', async (data: apiTypes.CredentialsCode) => {
  await api.verifyPhone(data)
})

export const handleRegister = createAsyncThunk('auth/register', async (data: apiTypes.CredentialsPassword) => {
  const token = await api.register(data)
  setToken(token)
  return token
})

export const handleLogout = createAsyncThunk('auth/logout', async (token: apiTypes.Token) => {
  removeToken()
  await api.logout(token)
})

export const handleRequestPasswordRecovery = createAsyncThunk(
  'auth/passwordRecoverRequest',
  async (data: apiTypes.CredentialsCode) => {
    await api.passwordRecoverRequest(data)
  },
)

export const handleVerifyPasswordRecoveryCode = createAsyncThunk(
  'auth/passwordRecoverCodeVerification',
  async (data: apiTypes.CredentialsCode) => {
    const res = await api.passwordRecoverCodeVerification(data)
    return res.data
  },
)

export const handleChangePassword = createAsyncThunk(
  'auth/passwordRecoverChangePassword',
  async (data: apiTypes.PasswordRecover) => {
    const res = await api.changePassword(data)
    return res.data
  },
)

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setCurrentStep(state, { payload }) {
      state.step = payload
    },
    setPhone(state, { payload }) {
      state.phone_number = payload
    },
    setCheckbox(state) {
      state.checkboxChecked = !state.checkboxChecked
    },
    updateField(state, { payload }) {
      const { name, value } = payload
      state[name] = value
    },
    setError(state, { payload }) {
      const { field, value } = payload
      state.errors[field] = value
    },
    clearAuthData(state) {
      state.phone_number = ''
      state.password = ''
      state.verificationCode = ''
      state.confirmPassword = ''
      state.step = 0
    },
  },
  extraReducers: (builder) => {
    builder.addCase(handleLogin.fulfilled, (state, { payload }) => {
      state.token = payload
      state.isAuth = true
    })
    builder.addCase(handleRefreshToken.fulfilled, (state, { payload }) => {
      state.token.access = payload
      state.isAuth = true
    })
    builder.addCase(handleRefreshToken.rejected, (state) => {
      state.token = {}
      state.isAuth = false
    })
    builder.addCase(handleGetVerificationCode.fulfilled, (state) => {
      state.step = 1
    })
    builder.addCase(handleVerifyPhone.fulfilled, (state) => {
      state.step = 2
    })
    builder.addCase(handleRegister.fulfilled, (state, { payload }) => {
      state.step = 0
      state.token = payload
      state.isAuth = true
    })
    builder.addCase(handleLogout.fulfilled, (state) => {
      state.step = 0
      state.token = {}
      state.isAuth = false
    })
    builder.addCase(handleRequestPasswordRecovery.fulfilled, (state) => {
      state.step = 1
    })
    builder.addCase(handleVerifyPasswordRecoveryCode.fulfilled, (state, { payload }) => {
      state.passwordRecoverSecretKey = payload.secret
      state.step = 2
    })
    builder.addCase(handleChangePassword.fulfilled, (state) => {
      state.step = 0
      state.redirect = NamePath.LOGIN
    })
  },
})

export const authActions = authSlice.actions

export default authSlice.reducer
